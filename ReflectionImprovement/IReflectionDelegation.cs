namespace ReflectionImprovement
{
    public interface IReflectionDelegation<TTarget>
    {
//        string ReflectionDelegationFunc(Target target, object parameter = null);
        TTarget ReflectionDelegationFunc(Target target, object parameter = null);
    }
}