using System;
using System.Reflection;

namespace ReflectionImprovement
{
    public class ReflectionDelegationWithGeneric<TTarget> : IReflectionDelegation<TTarget>
    {
        public readonly Func<TTarget> Func;

        public ReflectionDelegationWithGeneric(Target target, object parameter = null)
        {
            MethodInfo targetMethod = target.GetType().GetMethod("Call");
            Func = (Func<TTarget>) Delegate.CreateDelegate(typeof(Func<TTarget>), parameter, targetMethod);
        }


        public TTarget ReflectionDelegationFunc(Target target, object parameter = null)
        {
            return Func();
        }
    }
}