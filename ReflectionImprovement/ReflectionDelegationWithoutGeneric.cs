using System;
using System.Reflection;

namespace ReflectionImprovement
{

    public class ReflectionDelegationWithoutGeneric : IReflectionDelegation<string>
    {
        public string ReflectionDelegationFunc(Target target, object parameter = null)
        {
            MethodInfo targetMethod = typeof(Target).GetMethod("Call");
            CallBack @delegate = (CallBack) Delegate.CreateDelegate(typeof(CallBack), parameter, targetMethod);
            return @delegate();
        }
    }
}