using System;
using System.Linq.Expressions;
using System.Reflection;

namespace ReflectionImprovement
{
    public class ExpressionTreeImprovement<T>:IReflectionDelegation<string>
    {
        public string ReflectionDelegationFunc(Target target, object parameter = null)
        {
            Expression<Func<Target, string>> expression = (t) => t.Call();
            return expression.Compile().Invoke(target);
        }
    }
}