﻿using System;
using System.Diagnostics;

namespace ReflectionImprovement
{
    class Program
    {
        static void Main(string[] args)
        {
            Target target = new Target();
            
            Stopwatch stopwatch = new Stopwatch();
            
            stopwatch.Start();
            ReflectionDelegationWithoutGeneric reflectionDelegationWithoutGeneric =
                new ReflectionDelegationWithoutGeneric();
            
            string reflectionDelegationFuncWithoutGenericResult =
                reflectionDelegationWithoutGeneric.ReflectionDelegationFunc(target);
            stopwatch.Stop();
            
            Console.WriteLine(reflectionDelegationFuncWithoutGenericResult);
            Console.WriteLine($"reflectionDelegationFuncWithoutGenericResult: {stopwatch.ElapsedMilliseconds}");
            
            
            stopwatch.Restart();
            ReflectionDelegationWithGeneric<string> reflectionDelegationWithGeneric =
                new ReflectionDelegationWithGeneric<string>(target);

            string reflectionDelegationFuncWithGenericResult =
                reflectionDelegationWithGeneric.ReflectionDelegationFunc(target);
            stopwatch.Stop();
            
            Console.WriteLine(reflectionDelegationFuncWithGenericResult);
            Console.WriteLine($"reflectionDelegationFuncWithGenericResult: {stopwatch.ElapsedMilliseconds}");
            
            
            stopwatch.Restart();
            ExpressionTreeImprovement<string> expressionTreeImprovement = new ExpressionTreeImprovement<string>();
            string expressionTreeImprovementResult = expressionTreeImprovement.ReflectionDelegationFunc(target);
            stopwatch.Stop();
            Console.WriteLine(expressionTreeImprovementResult);
            Console.WriteLine($"expressionTreeImprovementResult: {stopwatch.ElapsedMilliseconds}");


        }
    }
}