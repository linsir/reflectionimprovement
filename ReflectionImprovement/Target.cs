namespace ReflectionImprovement
{
    public class Target
    {
        public string Property => "Test Target Property";

        public string Call()
        {
            return Property + "For Call";
        }
    }
}